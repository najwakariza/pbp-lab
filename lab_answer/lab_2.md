## Apakah perbedaan antara JSON dan XML?

<hr>

| JSON                                                                |                                                                                    XML |
| :------------------------------------------------------------------ | -------------------------------------------------------------------------------------: |
| JavaScript Object Notation                                          |                                                              Extensive Markup Language |
| Representasi Object JavaScript                                      |                                                                        Markup Language |
| Data oriented\_ yang disimpan seperti map dengan pasangan key value |                             _Document oriented_ yang disimpan sebagai _tree structure_ |
| Menguraikan data yang sedang dikirim                                |                                 Mengangkut data dari satu aplikasi ke aplikasi lainnya |
| Mendukung tipe data teks dan numerik                                | Mendukung banyak tipe data kompleks (bagan, _charts_, dan tipe data non-primitif lain) |
| Mendukung array yang bisa diakses                                   |                Harus menambahkan tag setiap item untuk mendukung array secara langsung |
| File berukuran lebih kecil                                          |                                                             File berukuran lebih besar |

## Apakah perbedaan antara HTML dan XML?

<hr>

| HTML                                            |                                                         XML |
| :---------------------------------------------- | ----------------------------------------------------------: |
| Hypertext Markup Languag                        |                                  Extensible Markup Language |
| Menampilkan data                                |                             Transportasi dan menyimpan data |
| Bahasa markup standar                           | Menyediakan sebuah framework untuk menentukan bahasa markup |
| Tidak _Case Sensitive_                          |                                            _Case Sensitive_ |
| Penulisan tag telah ditetapkan                  |  Penulisan tag tidak ditetapkan (bebas menulis tag sendiri) |
| Urutan (_nesting_) tidak terlalu diperhitungkan |             Urutan (_nesting_) harus dilakukan dengan benar |
| Tag penutup opsional                            |                                           Perlu tag penutup |
| Error kecil dapat diabaikan                     |                                       Tidak boleh ada error |

<hr>

## Referensi

- Resa Risyan. _Apa Perbedaan JSON Dan XML? - Monitor Teknologi_. 2021. Monitor Teknologi. https://www.monitorteknologi.com/perbedaan-json-dan-xml/.
- Azhar Krisna. 2021. _Apa Itu XML - JSON - Javascript - Front End & Back End_. Slideshare.Net. https://www.slideshare.net/AzharKrisna/apa-itu-xml-json-javascript-front-end-back-end.
- Sutiono S.Kom., M.Kom., M.T.I. 2021. _Perbedaan XML dan HTML: Fitur dan Kuncinya_. Dosen IT.com. https://dosenit.com/kuliah-it/pemrograman/perbedaan-xml-dan-html
