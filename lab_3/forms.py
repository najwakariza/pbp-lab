from django import forms
from lab_1.models import Friend

class FriendForm(forms.ModelForm):
    # specify the name of model to use
    class Meta:
        model = Friend
        fields = "__all__" # to indicate that all fields in the model should be used.
    