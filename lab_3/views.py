from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from lab_3.forms import FriendForm
from lab_1.models import Friend


# Create your views here.
@login_required(login_url='/admin/login')   # form only for authenticated user 
def index(request):
    friends = Friend.objects.all()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login')   # form only for authenticated user 
def add_friend(request):
    form = FriendForm()
    context = {'form' : form}
    if (request.method == 'POST'):
        form = FriendForm(request.POST or None) # create object of form
        # check if form data is valid
        if(form.is_valid()):
            form.save() # save the form data to model
        return HttpResponseRedirect('/lab-3')
    return render(request, 'lab3_form.html', context)