from django.contrib import admin
from .models import Note  # also to register needs imported class

# Register your models here.
admin.site.register(Note)