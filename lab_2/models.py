from django.db import models

# Create your models here.
class Note(models.Model):
    toUser = models.CharField(max_length=100)
    fromUser = models.CharField(max_length=100)
    title = models.CharField(max_length=100)
    message = models.CharField(max_length=300)