from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    # specifiy the name of model to use
    class Meta:
        model = Note
        fields = "__all__" # to indicate that all fields in the model should be used.