from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from lab_2.models import Note
from lab_4.forms import NoteForm


# Create your views here.
def index(request):
    notes = Note.objects.all()
    response = {'notes' :  notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    form = NoteForm()
    context = {'form' : form}
    if(request.method == 'POST'):
        # create object of form
        form = NoteForm(request.POST or None)
        # check if form data is valid
        if(form.is_valid()):
            form.save() # save the form data to model
            return HttpResponseRedirect('/lab-4')
    return render(request, 'lab4_form.html', context)

def note_list(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)